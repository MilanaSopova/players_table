 var array = [
    {
        "id": "16066",
        "imageUrl": "https://randomuser.me/api/portraits/men/84.jpg",
        "name": "Keshav",
        "score": 12
    },
        {
            "id": "16732",
            "imageUrl": "https://randomuser.me/api/portraits/men/89.jpg",
            "name": "فاطمة",
            "score": 140
        },
        {
            "id": "18423",
            "imageUrl": "https://randomuser.me/api/portraits/men/79.jpg",
            "name": "ÁnGgel",
            "score": 40
        },
        {
            "id": "19023",
            "imageUrl": "https://randomuser.me/api/portraits/men/81.jpg",
            "name": "Abhijit",
            "score": 10
        },
        {
            "id": "18712",
            "imageUrl": "https://randomuser.me/api/portraits/women/74.jpg",
            "name": "Gianfranco",
            "score": 40
        }
    ];
  var newArray = array.sort(function (a, b) {
     if (a.score > b.score) {
         return 1;
     }
     if (a.score < b.score) {
         return -1;
     }
      return 0;
 });

var table = document.getElementById("players");

 function addElement() {
     // debugger
     for (var i = 0; i < array.length; i++) {
         var newTr = document.createElement("tr");
         table.appendChild(newTr);
         var image = document.createElement("img");

         for (var key in array[i]) {
             var newTd = document.createElement("td");
             if(key == "imageUrl"){
                 image.src = array[i][key];
                 newTd.appendChild(image);
                 } else {
                     newTd.innerHTML = array[i][key];
                 }
             newTr.appendChild(newTd);
         }
     }
 }

 addElement(newArray);